package com.brom.epam.task14.config;

import com.brom.epam.task14.model.beans2.CatAnimal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
public class ProfilesConfig {
  @Bean
  @Profile("dev")
  public CatAnimal catAnimal() {
    return new CatAnimal();
  }
  @Bean
  @Profile("prod")
  public CatAnimal catAnimalProd() {
    return new CatAnimal();
  }
}
