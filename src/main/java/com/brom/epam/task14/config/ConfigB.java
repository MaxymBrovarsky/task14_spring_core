package com.brom.epam.task14.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Component;

@Configuration
@ComponentScans(
    {
        @ComponentScan (
            basePackages = "com.brom.epam.task14.model.beans2",
            excludeFilters = {
                @Filter(type = FilterType.REGEX, pattern = "^(?!.*Flower).*"),
            }
        ),
        @ComponentScan (
            basePackages = "com.brom.epam.task14.model.beans3",
            excludeFilters = {
                @Filter(type = FilterType.REGEX, pattern = "^(?!.*(.*BeanD)|(.*BeanF)).*")
            }
        )
    }
)
public class ConfigB {

}
