package com.brom.epam.task14.config;

import com.brom.epam.task14.model.otherbeans.OtherBeanA;
import com.brom.epam.task14.model.otherbeans.OtherBeanB;
import com.brom.epam.task14.model.otherbeans.OtherBeanC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OtherBeansConfig {
  @Bean
  public OtherBeanA otherBeanA() {
    return new OtherBeanA();
  }
  @Bean(name="otherBeanA2")
  public OtherBeanA otherBeanA2() {
    return new OtherBeanA();
  }
  @Bean(name="otherBeanA3")
  public OtherBeanA otherBeanA3() {
    return new OtherBeanA();
  }
  @Bean
  public OtherBeanB otherBeanB() {
    return new OtherBeanB();
  }
  @Bean
  public OtherBeanC otherBeanC() {
    return new OtherBeanC();
  }
}
