package com.brom.epam.task14.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.brom.epam.task14.model.beans1")
public class ConfigA {

}
