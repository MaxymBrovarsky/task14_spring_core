package com.brom.epam.task14;

import com.brom.epam.task14.view.View;

public class Application {

  public static void main(String[] args) {
    new View().show();
  }
}
