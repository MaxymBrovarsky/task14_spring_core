package com.brom.epam.task14.controller;

import com.brom.epam.task14.config.ConfigA;
import com.brom.epam.task14.config.ConfigB;
import com.brom.epam.task14.config.OtherBeansConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Controller {
  public String getAllBeansCreatedByConfigurationA() {
    return getAllBeansDefinedInConfig(ConfigA.class);
  }
  public String getAllBeansCreatedByConfigurationB() {
    return getAllBeansDefinedInConfig(ConfigB.class);
  }
  public String getAllBeansCreatedByOtherBeansConfig() {
    return getAllBeansDefinedInConfig(OtherBeansConfig.class);
  }



  private String getAllBeansDefinedInConfig(Class clazz) {
    ApplicationContext context = new AnnotationConfigApplicationContext(clazz);
    StringBuilder sb = new StringBuilder();
    for (String beanName: context.getBeanDefinitionNames()) {
      sb.append(beanName + "\n");
    }
    return sb.toString();
  }
}
