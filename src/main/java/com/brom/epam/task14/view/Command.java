package com.brom.epam.task14.view;

public interface Command {
  void execute();
}
