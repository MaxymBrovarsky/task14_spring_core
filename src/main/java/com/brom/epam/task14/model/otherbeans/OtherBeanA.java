package com.brom.epam.task14.model.otherbeans;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Primary
@Scope(scopeName = "singleton")
public class OtherBeanA implements OtherBean {

}
