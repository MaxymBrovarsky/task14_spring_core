package com.brom.epam.task14.model.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("C")
public class OtherBeanC implements OtherBean {

}
