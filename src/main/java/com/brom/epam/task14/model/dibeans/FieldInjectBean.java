package com.brom.epam.task14.model.dibeans;

import com.brom.epam.task14.model.otherbeans.OtherBeanA;
import com.brom.epam.task14.model.otherbeans.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class FieldInjectBean {
  @Autowired
  private OtherBeanB otherBeanB;
  @Autowired
  @Qualifier("otherBean2")
  private OtherBeanA otherBeanA;

  @Autowired
  @Qualifier("otherBean3")
  private OtherBeanA otherBeanA2;
}
