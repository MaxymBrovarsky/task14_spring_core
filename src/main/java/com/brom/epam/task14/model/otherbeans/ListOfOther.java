package com.brom.epam.task14.model.otherbeans;

import java.util.List;
import javax.print.attribute.standard.MediaSize.Other;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListOfOther {
  @Autowired
  private List<Other> others;
}
