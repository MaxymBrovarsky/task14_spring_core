package com.brom.epam.task14.model.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier("B")
@Scope(scopeName = "prototype")
public class OtherBeanB implements OtherBean {

}
