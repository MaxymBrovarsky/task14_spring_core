package com.brom.epam.task14.model.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class OtherBeans {
  @Autowired
  private OtherBean otherBeanA;
  @Autowired
  @Qualifier("B")
  private OtherBean otherBeanB;
  @Autowired
  @Qualifier("C")
  private OtherBean otherBeanC;
}
