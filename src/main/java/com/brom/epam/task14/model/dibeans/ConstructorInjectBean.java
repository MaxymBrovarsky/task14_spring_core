package com.brom.epam.task14.model.dibeans;

import com.brom.epam.task14.model.otherbeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;

public class ConstructorInjectBean {
  private OtherBeanC otherBeanC;

  @Autowired
  public ConstructorInjectBean(OtherBeanC otherBeanC) {
    this.otherBeanC = otherBeanC;
  }
}
