package com.brom.epam.task14.model.dibeans;

import com.brom.epam.task14.model.otherbeans.OtherBeanA;
import com.brom.epam.task14.model.otherbeans.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetterInjectBean {
  private OtherBeanA otherBeanA;

  @Autowired
  public void setOtherBeanA(OtherBeanA otherBeanA) {
    this.otherBeanA = otherBeanA;
  }
}
